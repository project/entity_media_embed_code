<?php

namespace Drupal\entity_media_embed_code\Form;

use Drupal\entity_media_embed_code\Plugin\media\Source\EmbedCode;
use Drupal\facets\Exception\Exception;
use Drupal\media_library\Form\AddFormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Creates a form to create media entities from oEmbed URLs.
 *
 * @internal
 *   Form classes are internal.
 */
class EmbedCodeForm extends AddFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->getBaseFormId() . '_embed_code';
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaType(FormStateInterface $form_state) {
    if ($this->mediaType) {
      return $this->mediaType;
    }

    $media_type = parent::getMediaType($form_state);
    if (!$media_type->getSource() instanceof EmbedCode) {
      throw new \InvalidArgumentException('Can only add media types which use an Embed Code source plugin.');
    }
    return $media_type;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    // Add a container to group the input elements for styling purposes.
    if (! isset($form['#attributes']['class'])) $form['#attributes']['class'] = [];
    $form['#attributes']['class'][] = 'media-library-add-form--embed-code';

    $form['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'media-library-add-form__input-wrapper',
        ],
      ],
    ];

    $form['container']['embed_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add @type via URL', [
        '@type' => $this->getMediaType($form_state)->label(),
      ]),
      '#description' => $this->t('Type the remote image url above.'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Place embed code here',
      ],
    ];

    $form['container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#submit' => ['::addButtonSubmit'],
      // @todo Move validation in https://www.drupal.org/node/2988215
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        // Add a fixed URL to post the form since AJAX forms are automatically
        // posted to <current> instead of $form['#action'].
        // @todo Remove when https://www.drupal.org/project/drupal/issues/2504115
        //   is fixed.
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];
    return $form;
  }


  /**
   * Submit handler for the add button.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state) {
    $this->processInputValues([$form_state->getValue('embed_code')], $form, $form_state);
  }

}
