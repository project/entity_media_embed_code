<?php

namespace Drupal\entity_media_embed_code\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'pvb_jw_player_id' widget.
 *
 * @FieldFormatter(
 *   id = "entity_media_embed_code_html",
 *   label = @Translation("Embed Code Html"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class MediaEntityEmbedCodeFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /**
     * @var int $delta
     * @var \Drupal\file\FileInterface $file
     */
    foreach ($items as $delta => $item) {

      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => "{{ text|raw }}",
        '#context' => [
          'text' => $item->value
        ]
      ];

    }

    return $elements;
  }

  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entityType = $field_definition->getTargetEntityTypeId();
    return $entityType == "media";
  }

}
