<?php

namespace Drupal\entity_media_embed_code\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;

use Drupal\media_entity_remote_image\RemoteImage\RemoteImageFetcher;
use Drupal\media_entity_remote_image\RemoteImage\RemoteImageException;

/**
 * Provides media type plugin for remote images.
 *
 * @MediaSource(
 *   id = "embed_code",
 *   label = @Translation("Embed code"),
 *   description = @Translation("Embed code."),
 *   allowed_field_types = {
 *     "string_long",
 *   },
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class EmbedCode extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  public function getSourceFieldConstraints() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    $media_url = $this->getSourceFieldValue($media);

    if (empty($media_url)) {
      return NULL;
    }
    switch ($name) {
      case 'default_name':
        $pattern = '/(?:<a|<iframe)\s+[^>]*\b(?:href|src)\s*=\s*["\']?([^"\'>\s]+)/i';
        preg_match($pattern, $media_url, $matches);
        $url = $matches[1] ?? NULL;
        if($url){
          // Parse the URL
          $parsedUrl = parse_url($url);
          // Reconstruct the URL without the query string and fragment
          $url = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . $parsedUrl['path'];
        }
        return $url ?: parent::getMetadata($media, 'default_name');
      default:
        break;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Insert Embed Code');
  }

}
